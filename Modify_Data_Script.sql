DO $$
	declare
	
	  cr1 CURSOR FOR 
		select o."EmploymentAccount_id", o."StrXml" 
		from 
		userdata."ParusBusinessSalaryCalculationOsn" o,
		userdata."ParusBusinessSalaryGenericSnu" s
		where o."Snu_id" = s.id 
		-- Здесь задается Вид выплаты (мнемокод)
		and s."Mnemo" = 'Отпуск'
		--
		and s."Type" = 'b774d32a-c4c5-4d09-8042-6fbf48818592'; 
		  
	  nId UUID;
	  nEmplId UUID;
	  nKind UUID;
	  nSchedule UUID;
 	  sText varchar(1000);
 	  aParams varchar array;
 	  sParam varchar(200);
 	  aParam varchar array;

 	 sStartDate varchar(20);
 	 sFinishDate varchar(20);
 	 
 	 dStartDate timestamp;
 	 dFinishDate timestamp;
 	
 	 sStartDateFor varchar(20);
 	 sFinishDateFor varchar(20);
 	 
 	 dStartDateFor timestamp;
 	 dFinishDateFor timestamp;
 	
	 
BEGIN

	CREATE EXTENSION IF NOT EXISTS "uuid-ossp";	
	
	-- вычисляем предустановленные значения

	-- ВидОтпуска
	  select id into nKind from userdata."ParusYugBusinessGenericPersonnelKindOfLeaveOfAbsence" k where k."Mnemo" = 'ЕжегОснОплОтпуск';
	  -- Календарный График
 	  select id into nSchedule from userdata."ParusYugBusinessGenericPersonnelScheduleOfWork" s where s."Mnemo" = 'Календарный';
	
	  OPEN cr1;
	  LOOP
	     FETCH cr1 INTO nEmplId, sText;
	     IF NOT FOUND THEN EXIT;END IF;
	    
	    -- разбираем строчку параметров
	    sText := Replace(sText, '<','');
	    sText := Replace(sText, '/>','');
	    sText := Replace(sText, 'params','');
	    sText := Trim(sText);
	    sText := Replace(sText, ' ','.');
	    aParams := string_to_array(sText,'.');
	   
		FOREACH sParam IN ARRAY aParams
		LOOP
			 sParam := Trim(sParam);
			 sParam := Replace(sParam, '=','.');
			 aParam := string_to_array(sParam,'.');
			
		 	sStartDate := '';
		 	sFinishDate := '';
		 	sStartDateFor := '';
		 	sFinishDateFor := '';
			 
			 if aParam[0] = 'BGN' then
			 	sStartDate := aParam[1];
			 elsif aParam[0] = 'END' then
			 	sFinishDate := aParam[1];
			 elsif aParam[0] = 'ForBGN' then
			 	sStartDateFor := aParam[1];
			 elsif aParam[0] = 'ForEND' then
			 	sFinishDateFor := aParam[1];
			 end if;
			
		END LOOP;	   
	
	  END LOOP;
	  CLOSE cr1;
	 
END $$;